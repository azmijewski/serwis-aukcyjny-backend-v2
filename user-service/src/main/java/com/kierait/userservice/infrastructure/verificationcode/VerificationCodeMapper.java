package com.kierait.userservice.infrastructure.verificationcode;

import com.kierait.userservice.domain.verificationcode.model.VerificationCode;
import com.kierait.userservice.infrastructure.user.UserMapper;
import com.kierait.util.mapper.MapperConfiguration;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfiguration.class, uses = UserMapper.class)
public interface VerificationCodeMapper {
    VerificationCode map(VerificationCodeEntity verificationCodeEntity);
    VerificationCodeEntity map(VerificationCode verificationCode);
}
