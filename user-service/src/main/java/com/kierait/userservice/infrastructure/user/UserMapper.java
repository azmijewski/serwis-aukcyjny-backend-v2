package com.kierait.userservice.infrastructure.user;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.mapper.MapperConfiguration;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfiguration.class)
public interface UserMapper {
    User map(UserEntity userEntity);
    UserEntity map(User user);
}
