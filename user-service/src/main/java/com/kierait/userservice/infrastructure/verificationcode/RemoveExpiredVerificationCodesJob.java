package com.kierait.userservice.infrastructure.verificationcode;

import com.kierait.userservice.domain.verificationcode.RemoveExpiredVerificationCodesCommand;
import com.kierait.util.handler.CommandDispatcher;
import lombok.RequiredArgsConstructor;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class RemoveExpiredVerificationCodesJob {
    private final CommandDispatcher commandDispatcher;

    @Scheduled(cron = "${remove-expired-codes-cron}")
    @SchedulerLock(name = "RemoveExpiredVerificationCodesJob_removeExpiredVerificationCodes")
    @Transactional
    void removeExpiredVerificationCodes() {
        commandDispatcher.dispatch(new RemoveExpiredVerificationCodesCommand());
    }
}
