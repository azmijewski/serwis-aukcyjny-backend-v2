package com.kierait.userservice.infrastructure.address;

public enum AddressType {
    MAIN,
    CONTACT,
    ADDITIONAL,
}
