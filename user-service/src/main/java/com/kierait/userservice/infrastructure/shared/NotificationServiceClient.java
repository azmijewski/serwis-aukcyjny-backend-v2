package com.kierait.userservice.infrastructure.shared;

import com.kierait.util.feign.GlobalFeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "NOTIFICATION-SERVICE", configuration = GlobalFeignConfig.class)
interface NotificationServiceClient {
    @PostMapping("/messages")
    ScheduleMessageResponse scheduleMessage(@RequestBody ScheduleMessageRequest scheduleMessageRequest);
}
