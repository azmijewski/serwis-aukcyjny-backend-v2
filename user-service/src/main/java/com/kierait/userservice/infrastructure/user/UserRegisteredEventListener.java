package com.kierait.userservice.infrastructure.user;

import com.kierait.userservice.domain.user.SendUserRegisteredNotificationCommand;
import com.kierait.userservice.domain.user.UserDao;
import com.kierait.userservice.domain.user.event.UserRegisteredEvent;
import com.kierait.userservice.domain.verificationcode.CreateVerificationCodeForUserCommand;
import com.kierait.util.handler.CommandDispatcher;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.function.Consumer;

@Configuration
@RequiredArgsConstructor
class UserRegisteredEventListener {
    private final CommandDispatcher commandDispatcher;
    private final UserDao userDao;
    private final TransactionTemplate transactionTemplate;

    @Bean
    @Transactional
    Consumer<UserRegisteredEvent> consumeUserRegisteredEvent() {
        return userRegisteredEvent ->
                transactionTemplate.executeWithoutResult(transactionStatus -> {
                    var user = userDao.getById(userRegisteredEvent.getUserId());
                    var verificationCode = commandDispatcher.dispatchAndGet(
                            new CreateVerificationCodeForUserCommand(user)
                    );
                    commandDispatcher.dispatchAndGet(
                            new SendUserRegisteredNotificationCommand(verificationCode)
                    );
                });
    }
}

