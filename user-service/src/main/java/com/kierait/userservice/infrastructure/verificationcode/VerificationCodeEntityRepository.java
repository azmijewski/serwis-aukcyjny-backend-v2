package com.kierait.userservice.infrastructure.verificationcode;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Repository
interface VerificationCodeEntityRepository extends CrudRepository<VerificationCodeEntity, Long> {
    Optional<VerificationCodeEntity> findByCode(String code);
    Set<VerificationCodeEntity> findAllByExpirationAtBefore(LocalDateTime now);
    Set<VerificationCodeEntity> findAllByUserId(Long userId);
}
