package com.kierait.userservice.infrastructure.address;

import com.kierait.userservice.infrastructure.user.UserEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Entity
@Builder
@Table(name = "address")
public class AddressEntity {
    private static final String SEQUENCE_NAME = "address_id_sequence";

    @Id
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    private Long id;

    private String city;

    private String street;

    private String number;

    private String apartment;

    private String postCode;

    private LocalDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private AddressType type;

    @ManyToOne(optional = false)
    @Setter
    private UserEntity user;
}
