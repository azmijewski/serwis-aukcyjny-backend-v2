package com.kierait.userservice.infrastructure.user;

import com.kierait.userservice.domain.user.AuthenticationUserDao;
import com.kierait.userservice.domain.user.model.User;
import com.kierait.userservice.infrastructure.config.properties.KeycloakProperties;
import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.Optional;

@Component
@RequiredArgsConstructor
class AuthenticationUserDaoImpl implements AuthenticationUserDao {
    private final Keycloak keycloak;
    private final KeycloakProperties keycloakProperties;
    private final KeycloakMapper keycloakMapper;

    @Override
    public Optional<String> registerUser(User user, String password) {
        var userRepresentation = keycloakMapper.map(user, password);
        var response = keycloak.realm(keycloakProperties.getRealm()).users()
                .create(userRepresentation);
        if (response.getStatus() != Response.Status.CREATED.getStatusCode()) {
            return Optional.empty();
        }
        return Optional.of(response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1"));
    }

    @Override
    public void verifyUser(User user) {
        var userRepresentation = getUserResource(user.getGlobalId()).toRepresentation();
        userRepresentation.setEmailVerified(true);
        getUserResource(user.getGlobalId()).update(userRepresentation);
    }

    @Override
    public void logoutUser(User user) {
        getUserResource(user.getGlobalId()).logout();
    }

    private UserResource getUserResource(String globalUserId) {
        return keycloak.realm(keycloakProperties.getRealm())
                .users()
                .get(globalUserId);
    }
}
