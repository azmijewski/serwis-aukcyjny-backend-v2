package com.kierait.userservice.infrastructure.user;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.mapper.MapperConfiguration;
import org.keycloak.OAuth2Constants;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(config = MapperConfiguration.class)
interface KeycloakMapper {

    @Mappings({
            @Mapping(target = "username", source = "user.email"),
            @Mapping(target = "enabled", constant = "true"),
            @Mapping(target = "emailVerified", constant = "false"),
            @Mapping(target = "id", ignore = true)
    })
    UserRepresentation map(User user, String password);

    @AfterMapping
    default void mapPassword(String password, @MappingTarget UserRepresentation userRepresentation) {
        var credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(OAuth2Constants.PASSWORD);
        credentialRepresentation.setValue(password);
        credentialRepresentation.setTemporary(false);
        userRepresentation.setCredentials(List.of(credentialRepresentation));
    }
}
