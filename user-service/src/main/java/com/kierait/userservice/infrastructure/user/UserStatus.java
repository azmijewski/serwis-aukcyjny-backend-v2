package com.kierait.userservice.infrastructure.user;

public enum UserStatus {
    REGISTERED, CONFIRMED, DELETED, BLOCKED
}
