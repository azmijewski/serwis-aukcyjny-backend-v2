package com.kierait.userservice.infrastructure.address;

import com.kierait.userservice.domain.address.AddressDao;
import com.kierait.userservice.domain.address.model.Address;
import com.kierait.userservice.infrastructure.user.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional(propagation = Propagation.MANDATORY)
@RequiredArgsConstructor
class AddressDaoImpl implements AddressDao {
    private final AddressMapper addressMapper;
    private final UserMapper userMapper;
    private final AddressEntityRepository addressEntityRepository;

    @Override
    public Address save(Address address) {
        var addressToSave = addressMapper.map(address);
        addressToSave.setUser(userMapper.map(address.getUser()));
        return addressMapper.map(addressEntityRepository.save(addressToSave));
    }
}
