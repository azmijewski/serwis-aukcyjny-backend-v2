package com.kierait.userservice.infrastructure.shared;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kierait.userservice.domain.shared.ExternalNotificationSender;
import com.kierait.userservice.domain.shared.NotificationType;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
class ExternalNotificationSenderImpl implements ExternalNotificationSender {
    private final NotificationServiceClient notificationServiceClient;
    private final ObjectMapper objectMapper;

    @Override
    public void sendNotification(String recipient, NotificationType type, Map<String, Object> payload) {
        try {
            var request = createRequest(recipient, type, payload);
            log.debug("Schedule message request: {}", request);
            var response = notificationServiceClient.scheduleMessage(request);
            log.debug("Schedule message response: {}", response);
        } catch (JsonProcessingException e) {
            log.error("Schedule message error while creating payload", e);
        } catch (FeignException e) {
            log.error("Schedule message error while sending to notification-service", e);
        }
    }

    private ScheduleMessageRequest createRequest(String recipient, NotificationType type, Map<String, Object> payload) throws JsonProcessingException {
        return ScheduleMessageRequest.builder()
                .payload(objectMapper.writeValueAsString(payload))
                .recipient(recipient)
                .type(type.name())
                .build();
    }
}
