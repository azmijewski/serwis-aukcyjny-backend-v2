package com.kierait.userservice.infrastructure.user;

import com.kierait.userservice.domain.user.UserDao;
import com.kierait.userservice.domain.user.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Component
@Transactional(propagation = Propagation.MANDATORY)
@RequiredArgsConstructor
class UserDaoImpl implements UserDao {
    private final UserEntityRepository userEntityRepository;
    private final UserMapper userMapper;

    @Override
    public User getByGlobalId(String globalUserId) {
        return userMapper.map(userEntityRepository.getByGlobalId(globalUserId));
    }

    @Override
    public boolean existsByEmail(String email) {
        return userEntityRepository.existsByEmail(email);
    }

    @Override
    public User save(User user) {
        var userToSave = userMapper.map(user);
        return userMapper.map(userEntityRepository.save(userToSave));
    }

    @Override
    public User getById(Long id) {
        return userEntityRepository.findById(id)
                .map(userMapper::map)
                .orElse(null);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userEntityRepository.findByEmail(email)
                .map(userMapper::map);
    }
}
