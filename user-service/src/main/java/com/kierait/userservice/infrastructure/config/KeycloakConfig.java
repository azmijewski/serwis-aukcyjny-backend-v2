package com.kierait.userservice.infrastructure.config;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import com.kierait.userservice.infrastructure.config.properties.KeycloakProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class KeycloakConfig {

    @Bean
    Keycloak keycloakClient(KeycloakProperties keycloakProperties) {
        return KeycloakBuilder.builder()
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(keycloakProperties.getClientId())
                .username(keycloakProperties.getUsername())
                .password(keycloakProperties.getPassword())
                .serverUrl(keycloakProperties.getServerUrl())
                .realm(keycloakProperties.getMasterRealm())
                .build();
    }
}
