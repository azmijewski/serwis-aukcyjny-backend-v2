package com.kierait.userservice.infrastructure.user;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserEntityRepository extends CrudRepository<UserEntity, Long> {
    UserEntity getByGlobalId(String globalId);
    boolean existsByEmail(String email);
    Optional<UserEntity> findByEmail(String email);
}
