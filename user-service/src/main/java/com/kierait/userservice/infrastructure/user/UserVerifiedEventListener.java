package com.kierait.userservice.infrastructure.user;

import com.kierait.userservice.domain.user.SendUserVerifiedNotificationCommand;
import com.kierait.userservice.domain.user.UserDao;
import com.kierait.userservice.domain.verificationcode.event.UserVerifiedEvent;
import com.kierait.util.handler.CommandDispatcher;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.function.Consumer;

@Configuration
@RequiredArgsConstructor
class UserVerifiedEventListener {
    private final UserDao userDao;
    private final CommandDispatcher commandDispatcher;
    private final TransactionTemplate transactionTemplate;

    @Bean
    Consumer<UserVerifiedEvent> consumeAccountConfirmedEvent() {
        return userVerifiedEvent -> transactionTemplate.executeWithoutResult(transactionStatus -> {
            var user = userDao.getById(userVerifiedEvent.getUserId());
            commandDispatcher.dispatchAndGet(
                    new SendUserVerifiedNotificationCommand(user)
            );
        });
    }
}
