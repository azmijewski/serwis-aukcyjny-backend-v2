package com.kierait.userservice.infrastructure.shared;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
class ScheduleMessageRequest {
    String recipient;
    String payload;
    String type;
}
