package com.kierait.userservice.infrastructure.shared;

import lombok.Value;

import java.time.LocalDateTime;

@Value
class ScheduleMessageResponse {
    Long id;
    LocalDateTime createdAt;
}
