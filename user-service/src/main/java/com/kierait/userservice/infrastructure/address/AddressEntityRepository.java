package com.kierait.userservice.infrastructure.address;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface AddressEntityRepository extends CrudRepository<AddressEntity, Long> {
}
