package com.kierait.userservice.infrastructure.address;

import com.kierait.userservice.domain.address.model.Address;
import com.kierait.userservice.infrastructure.user.UserMapper;
import com.kierait.util.mapper.MapperConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(config = MapperConfiguration.class, uses = UserMapper.class)
public interface AddressMapper {
    @Mappings({
            @Mapping(target = "user", ignore = true)
    })
    Address map(AddressEntity address);
    AddressEntity map(Address address);
}
