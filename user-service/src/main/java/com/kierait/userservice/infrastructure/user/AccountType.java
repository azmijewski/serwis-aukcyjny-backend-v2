package com.kierait.userservice.infrastructure.user;

public enum AccountType {
    NORMAL, PREMIUM
}
