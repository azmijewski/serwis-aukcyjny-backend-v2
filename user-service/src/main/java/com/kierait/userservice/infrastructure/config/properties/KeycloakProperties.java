package com.kierait.userservice.infrastructure.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "keycloak")
public class KeycloakProperties {
    private String clientId;
    private String serverUrl;
    private String realm;
    private String masterRealm;
    private String username;
    private String password;
}
