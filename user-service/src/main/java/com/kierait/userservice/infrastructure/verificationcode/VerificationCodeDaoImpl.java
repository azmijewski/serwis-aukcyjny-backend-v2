package com.kierait.userservice.infrastructure.verificationcode;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.userservice.domain.verificationcode.VerificationCodeDao;
import com.kierait.userservice.domain.verificationcode.model.VerificationCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Transactional(propagation = Propagation.MANDATORY)
@RequiredArgsConstructor
class VerificationCodeDaoImpl implements VerificationCodeDao {
    private final VerificationCodeEntityRepository verificationCodeEntityRepository;
    private final VerificationCodeMapper verificationCodeMapper;
    private final Clock clock;

    @Override
    public Optional<VerificationCode> findByCode(String code) {
        return verificationCodeEntityRepository.findByCode(code)
                .map(verificationCodeMapper::map);
    }

    @Override
    public Set<VerificationCode> findAllExpired() {
        var now = LocalDateTime.now(clock);
        return verificationCodeEntityRepository.findAllByExpirationAtBefore(now)
                .stream()
                .map(verificationCodeMapper::map)
                .collect(Collectors.toSet());
    }

    @Override
    public VerificationCode save(VerificationCode verificationCode) {
        var codeToSave = verificationCodeMapper.map(verificationCode);
        return verificationCodeMapper.map(verificationCodeEntityRepository.save(codeToSave));
    }

    @Override
    public void deleteAll(Set<VerificationCode> verificationCodes) {
        var verificationCodesToDelete = verificationCodes.stream()
                .map(verificationCodeMapper::map)
                .collect(Collectors.toSet());
        verificationCodeEntityRepository.deleteAll(verificationCodesToDelete);
    }

    @Override
    public Set<VerificationCode> findAllByUser(User user) {
        return verificationCodeEntityRepository.findAllByUserId(user.getId()).stream()
                .map(verificationCodeMapper::map)
                .collect(Collectors.toSet());
    }
}
