package com.kierait.userservice.api.config;

import com.kierait.util.api.ApiCommandDispatcher;
import com.kierait.util.handler.CommandDispatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApiDispatchersConfig {
    @Bean
    ApiCommandDispatcher apiCommandDispatcher(CommandDispatcher commandDispatcher) {
        return new ApiCommandDispatcher(commandDispatcher);
    }
}
