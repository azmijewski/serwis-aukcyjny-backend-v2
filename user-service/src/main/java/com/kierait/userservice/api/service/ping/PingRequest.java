package com.kierait.userservice.api.service.ping;

import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class PingRequest {
    @NotBlank
    String message;
}
