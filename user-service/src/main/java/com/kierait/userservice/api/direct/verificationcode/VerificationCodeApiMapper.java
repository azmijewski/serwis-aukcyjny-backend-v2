package com.kierait.userservice.api.direct.verificationcode;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.userservice.domain.verificationcode.ResendVerificationCodeCommand;
import com.kierait.userservice.domain.verificationcode.VerifyUserByVerificationCodeCommand;
import com.kierait.util.mapper.MapperConfiguration;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfiguration.class)
public interface VerificationCodeApiMapper {
    VerifyUserByVerificationCodeCommand map(VerifyAccountRequest request);
    VerifyAccountResponse map(User user);
    ResendVerificationCodeCommand map(ResendVerificationCodeRequest request);
}
