package com.kierait.userservice.api.direct.verificationcode;

import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class VerifyAccountRequest {
    @NotBlank
    String code;
}
