package com.kierait.userservice.api.direct.verificationcode;

import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Value
public class ResendVerificationCodeRequest {
    @Email
    @NotBlank
    String email;
}
