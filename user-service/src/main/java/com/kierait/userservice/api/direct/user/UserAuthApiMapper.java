package com.kierait.userservice.api.direct.user;

import com.kierait.userservice.domain.user.RegisterUserCommand;
import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.mapper.MapperConfiguration;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfiguration.class)
public interface UserAuthApiMapper {
    RegisterUserCommand map(RegisterUserRequest request);
    RegisterUserResponse map(User user);
}
