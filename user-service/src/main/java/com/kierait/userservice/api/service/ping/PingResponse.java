package com.kierait.userservice.api.service.ping;

import lombok.Value;

@Value
public class PingResponse {
    String message;
}
