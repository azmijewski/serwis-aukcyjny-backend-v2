package com.kierait.userservice.api.shared;

import com.kierait.util.api.ExceptionApi;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionResponseApi extends ExceptionApi {
}
