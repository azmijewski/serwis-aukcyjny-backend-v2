package com.kierait.userservice.api.direct.user;

import com.kierait.userservice.domain.user.LogoutUserCommand;
import com.kierait.util.api.ApiCommandDispatcher;
import com.kierait.util.api.ApiErrorResponse;
import com.kierait.util.security.SecurityContextProvider;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@Tag(name = "user-auth")
@RequiredArgsConstructor
public class UserAuthApi {
    private final ApiCommandDispatcher apiCommandDispatcher;
    private final UserAuthApiMapper userAuthApiMapper;
    private final SecurityContextProvider securityContextProvider;

    @PostMapping("/registration")
    @Operation(summary = "Verify account",
            responses = {
                    @ApiResponse(responseCode = "200", description = "User successfully registered", content = @Content(schema = @Schema(implementation = RegisterUserResponse.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))),
                    @ApiResponse(responseCode = "409", description = "Email already exists", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))),
            })
    ResponseEntity<?> registerUser(@RequestBody RegisterUserRequest request) {
        var command = userAuthApiMapper.map(request);
        return apiCommandDispatcher.dispatch(command, userAuthApiMapper::map);
    }

    @PostMapping("/logout")
    @Operation(summary = "Verify account",
            responses = {
                    @ApiResponse(responseCode = "200", description = "User successfully logout")
            })
    ResponseEntity<?> logout() {
        var securityContext = securityContextProvider.getCurrentContext();
        return apiCommandDispatcher.dispatch(new LogoutUserCommand(securityContext.getId()));
    }

}
