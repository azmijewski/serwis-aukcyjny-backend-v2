package com.kierait.userservice.api.direct.verificationcode;

import lombok.Value;

@Value
public class VerifyAccountResponse {
    String firstName;
    String lastName;
    String email;
}
