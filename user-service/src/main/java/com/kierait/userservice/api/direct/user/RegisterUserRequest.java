package com.kierait.userservice.api.direct.user;

import lombok.Value;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Value
public class RegisterUserRequest {
    @NotBlank
    @Email
    String email;

    @NotBlank
    String firstName;

    @NotBlank
    String lastName;

    @NotBlank
    @PESEL
    String pesel;

    @NotBlank
    String phoneNumber;

    @NotBlank
    String password;

    @NotBlank
    String city;

    @NotBlank
    String street;

    @NotBlank
    String number;

    @NotBlank
    String apartment;

    @NotBlank
    String postCode;
}
