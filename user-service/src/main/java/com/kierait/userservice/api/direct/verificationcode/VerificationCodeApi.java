package com.kierait.userservice.api.direct.verificationcode;

import com.kierait.util.api.ApiCommandDispatcher;
import com.kierait.util.api.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/verification")
@Tag(name = "verification-code")
@RequiredArgsConstructor
public class VerificationCodeApi {
    private final ApiCommandDispatcher apiCommandDispatcher;
    private final VerificationCodeApiMapper verificationCodeApiMapper;

    @PostMapping
    @Operation(summary = "Verify account",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Account successfully verified", content = @Content(schema = @Schema(implementation = VerifyAccountResponse.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Verification code not found or expired", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))),
                    @ApiResponse(responseCode = "409", description = "User cannot be verified", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))),
            })
    ResponseEntity<?> verifyAccount(@RequestBody @Valid VerifyAccountRequest verifyAccountRequest) {
        var command = verificationCodeApiMapper.map(verifyAccountRequest);
        return apiCommandDispatcher.dispatch(command, verificationCodeApiMapper::map);
    }

    @PostMapping("/resending")
    @Operation(summary = "Resend verification code",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Account successfully verified", content = @Content(schema = @Schema(implementation = VerifyAccountResponse.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))),
            })
    ResponseEntity<?> resendVerificationCode(@RequestBody @Valid ResendVerificationCodeRequest resendVerificationCodeRequest) {
        var command = verificationCodeApiMapper.map(resendVerificationCodeRequest);
        return apiCommandDispatcher.dispatch(command);
    }
}
