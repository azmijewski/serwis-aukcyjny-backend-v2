package com.kierait.userservice.api.direct.user;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RegisterUserResponse {
    String email;
    String firstName;
    String lastName;
    String pesel;
    String phoneNumber;
}
