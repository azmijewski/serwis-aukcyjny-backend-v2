package com.kierait.userservice;

import com.kierait.util.annotations.EnableDispatchers;
import com.kierait.util.annotations.EnableFeignOAuth2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ConfigurationPropertiesScan(basePackageClasses = UserServiceApplication.class)
@EnableFeignClients
@EnableFeignOAuth2
@EnableDispatchers
@EnableAsync
public class UserServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }
}
