package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.verificationcode.model.VerificationCode;
import com.kierait.util.handler.Command;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
public class SendUserRegisteredNotificationCommand implements Command<Void> {
    @NotNull
    VerificationCode verificationCode;
}
