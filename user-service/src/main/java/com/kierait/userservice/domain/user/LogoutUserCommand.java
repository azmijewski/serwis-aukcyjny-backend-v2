package com.kierait.userservice.domain.user;

import com.kierait.util.handler.Command;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class LogoutUserCommand implements Command<Void> {
    @NotBlank
    String globalUserId;
}
