package com.kierait.userservice.domain.user.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.With;

import java.time.LocalDateTime;


@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@Builder
public class User {
    private Long id;

    @With
    private String globalId;

    private String email;

    private String firstName;

    private String lastName;

    private String pesel;

    private String phoneNumber;

    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

    @Builder.Default
    private UserStatus status = UserStatus.REGISTERED;

    @Builder.Default
    private AccountType accountType = AccountType.NORMAL;

    public String getDisplayName() {
        return "%s %s".formatted(firstName, lastName);
    }

    public void verify() {
        status = UserStatus.CONFIRMED;
    }

    public boolean canBeVerified() {
        return status == UserStatus.REGISTERED;
    }
}
