package com.kierait.userservice.domain.verificationcode;

import com.kierait.userservice.domain.config.property.VerificationCodeProperties;
import com.kierait.userservice.domain.user.model.User;
import com.kierait.userservice.domain.verificationcode.model.VerificationCode;
import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.LocalDateTime;

@Handler
@RequiredArgsConstructor
class CreateVerificationCodeForUserCommandHandler implements CommandHandler<CreateVerificationCodeForUserCommand, VerificationCode> {
    private final VerificationCodeDao verificationCodeDao;
    private final VerificationCodeProperties verificationCodeProperties;
    private final VerificationCodeGenerator verificationCodeGenerator;
    private final Clock clock;

    @Override
    @Transactional
    public HandlerResult<VerificationCode> handle(CreateVerificationCodeForUserCommand command) {
        var verificationCode = createVerificationCode(command.getUser());
        var createdCode = verificationCodeDao.save(verificationCode);
        return HandlerResult.success(createdCode);
    }

    @Override
    public Class<? extends Command<VerificationCode>> commandClass() {
        return CreateVerificationCodeForUserCommand.class;
    }

    private VerificationCode createVerificationCode(User user) {
        return VerificationCode.builder()
                .code(verificationCodeGenerator.generate())
                .expirationAt(LocalDateTime.now(clock).plus(verificationCodeProperties.getValidityTime()))
                .user(user)
                .build();
    }
}
