package com.kierait.userservice.domain.user.event;

import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
public class UserRegisteredEvent {
    @NotNull
    Long userId;
}
