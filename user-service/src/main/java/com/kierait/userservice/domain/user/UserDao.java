package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.user.model.User;

import java.util.Optional;

public interface UserDao {
    User getByGlobalId(String globalUserId);
    boolean existsByEmail(String email);
    User save(User user);
    User getById(Long id);
    Optional<User> findByEmail(String email);
}
