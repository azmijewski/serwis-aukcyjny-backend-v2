package com.kierait.userservice.domain.exception;

import com.kierait.util.exception.NotFoundException;

public class VerificationCodeNotFoundException extends NotFoundException {
    public VerificationCodeNotFoundException() {
        super("Verification code not found", "VERIFICATION_CODE_NOT_FOUND");
    }
}
