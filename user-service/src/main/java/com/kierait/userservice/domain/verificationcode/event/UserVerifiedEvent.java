package com.kierait.userservice.domain.verificationcode.event;

import lombok.Value;

@Value
public class UserVerifiedEvent {
    Long userId;
}
