package com.kierait.userservice.domain.verificationcode;

public interface VerificationCodeGenerator {
    String generate();
}
