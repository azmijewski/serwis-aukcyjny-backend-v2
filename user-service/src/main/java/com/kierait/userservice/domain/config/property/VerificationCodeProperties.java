package com.kierait.userservice.domain.config.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@Data
@ConfigurationProperties(prefix = "verification-code")
public class VerificationCodeProperties {
    private Duration validityTime = Duration.ofHours(24);
}
