package com.kierait.userservice.domain.verificationcode;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
class UUIDVerificationCodeGenerator implements VerificationCodeGenerator {
    @Override
    public String generate() {
        return UUID.randomUUID().toString();
    }
}
