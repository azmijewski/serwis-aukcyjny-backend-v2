package com.kierait.userservice.domain.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class EventConstants {
    public static final String USER_REGISTERED_EVENT_DESTINATION = "consumeUserRegisteredEvent-out-0";
    public static final String USER_VERIFIED_EVENT_DESTINATION = "consumeAccountConfirmedEvent-out-0";

}
