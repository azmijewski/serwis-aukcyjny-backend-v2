package com.kierait.userservice.domain.verificationcode.model;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.clock.DefaultClock;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDateTime;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@Builder
public class VerificationCode {
    private Long id;

    private String code;

    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();

    private LocalDateTime expirationAt;

    private User user;

    public boolean isNotExpired() {
        return expirationAt.isAfter(LocalDateTime.now(DefaultClock.getInstance()));
    }
}
