package com.kierait.userservice.domain.address.model;

import com.kierait.userservice.domain.user.model.User;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.With;

import java.time.LocalDateTime;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@Builder
public class Address {
    private String city;
    private String street;
    private String number;
    private String apartment;
    private String postCode;
    private AddressType type;
    @With
    private User user;
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();
}
