package com.kierait.userservice.domain.shared;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Map;

public interface ExternalNotificationSender {
    void sendNotification(@NotBlank String recipient, @NotNull NotificationType type, @NotNull Map<String, Object> payload);
}
