package com.kierait.userservice.domain.user.model;

public enum AccountType {
    NORMAL, PREMIUM
}
