package com.kierait.userservice.domain.config;

import com.kierait.util.clock.DefaultClock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

@Configuration
class ClockConfig {
    @Bean
    Clock clock() {
        return DefaultClock.getInstance();
    }
}
