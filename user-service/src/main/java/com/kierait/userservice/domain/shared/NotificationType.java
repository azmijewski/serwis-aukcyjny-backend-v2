package com.kierait.userservice.domain.shared;

public enum NotificationType {
    AFTER_REGISTRATION,
    ACCOUNT_CONFIRMED,
}
