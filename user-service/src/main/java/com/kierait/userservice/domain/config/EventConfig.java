package com.kierait.userservice.domain.config;

import com.kierait.util.event.EventPublisher;
import com.kierait.util.event.QueueEventPublisher;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@Configuration
public class EventConfig {
    @Bean
    EventPublisher eventPublisher(StreamBridge streamBridge) {
        return new QueueEventPublisher(streamBridge);
    }
}
