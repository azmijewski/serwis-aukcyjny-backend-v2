package com.kierait.userservice.domain.verificationcode;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.userservice.domain.verificationcode.model.VerificationCode;
import com.kierait.util.handler.Command;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
public class CreateVerificationCodeForUserCommand implements Command<VerificationCode> {
    @NotNull
    User user;
}
