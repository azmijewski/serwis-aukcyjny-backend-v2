package com.kierait.userservice.domain.verificationcode;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.handler.Command;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class VerifyUserByVerificationCodeCommand implements Command<User> {
    @NotBlank
    String code;
}

