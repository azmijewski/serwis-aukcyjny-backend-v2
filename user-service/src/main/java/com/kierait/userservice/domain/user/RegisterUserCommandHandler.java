package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.address.AddressDao;
import com.kierait.userservice.domain.address.model.Address;
import com.kierait.userservice.domain.address.model.AddressType;
import com.kierait.userservice.domain.exception.UserAlreadyExistConflictException;
import com.kierait.userservice.domain.user.event.UserRegisteredEvent;
import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.annotations.Handler;
import com.kierait.util.event.EventPublisher;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import static com.kierait.userservice.domain.config.EventConstants.USER_REGISTERED_EVENT_DESTINATION;

@Handler
@RequiredArgsConstructor
class RegisterUserCommandHandler implements CommandHandler<RegisterUserCommand, User> {
    private final UserDao userDao;
    private final AuthenticationUserDao authenticationUserDao;
    private final AddressDao addressDao;
    private final EventPublisher eventPublisher;

    @Override
    @Transactional
    public HandlerResult<User> handle(RegisterUserCommand command) {
        if (userDao.existsByEmail(command.getEmail())) {
            return HandlerResult.error(new UserAlreadyExistConflictException(command.getEmail()));
        }
        var user = createUser(command);
        var address = createAddress(command);
        return authenticationUserDao.registerUser(user, command.getPassword())
                .map(globalUserId -> saveUser(user.withGlobalId(globalUserId), address))
                .orElse(HandlerResult.error(new UserAlreadyExistConflictException(user.getEmail())));
    }

    @Override
    public Class<? extends Command<User>> commandClass() {
        return RegisterUserCommand.class;
    }

    private User createUser(RegisterUserCommand registerUserCommand) {
        return User.builder()
                .email(registerUserCommand.getEmail())
                .firstName(registerUserCommand.getFirstName())
                .lastName(registerUserCommand.getLastName())
                .pesel(registerUserCommand.getPesel())
                .phoneNumber(registerUserCommand.getPhoneNumber())
                .build();
    }

    private Address createAddress(RegisterUserCommand command) {
        return Address.builder()
                .city(command.getCity())
                .street(command.getStreet())
                .number(command.getNumber())
                .apartment(command.getApartment())
                .postCode(command.getPostCode())
                .type(AddressType.MAIN)
                .build();
    }

    private HandlerResult<User> saveUser(User user, Address address) {
        user = userDao.save(user);
        address = address.withUser(user);
        addressDao.save(address);
        notifyAboutCreatedUser(user);
        return HandlerResult.success(user);
    }

    private void notifyAboutCreatedUser(User user) {
        eventPublisher.publishEvent(
                USER_REGISTERED_EVENT_DESTINATION,
                new UserRegisteredEvent(user.getId())
        );
    }
}
