package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.handler.Command;
import lombok.Value;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Value
public class RegisterUserCommand implements Command<User> {
    @NotBlank
    @Email
    String email;

    @NotBlank
    String firstName;

    @NotBlank
    String lastName;

    @NotBlank
    @PESEL
    String pesel;

    @NotBlank
    String phoneNumber;

    @NotBlank
    String password;

    @NotBlank
    String city;

    @NotBlank
    String street;

    @NotBlank
    String number;

    @NotBlank
    String apartment;

    @NotBlank
    String postCode;
}
