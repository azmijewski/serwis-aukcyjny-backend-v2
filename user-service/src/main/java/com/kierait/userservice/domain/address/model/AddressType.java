package com.kierait.userservice.domain.address.model;

public enum AddressType {
    MAIN,
    CONTACT,
    ADDITIONAL,
}
