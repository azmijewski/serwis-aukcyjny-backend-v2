package com.kierait.userservice.domain.user.model;

public enum UserStatus {
    REGISTERED, CONFIRMED, DELETED, BLOCKED
}
