package com.kierait.userservice.domain.config.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "app.url")
public class AppUrlProperties {
    private String appUrl;
    private String confirmAccountUrl;
}
