package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.shared.ExternalNotificationSender;
import com.kierait.userservice.domain.shared.NotificationType;
import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Handler
@RequiredArgsConstructor
class SendUserVerifiedNotificationCommandHandler implements CommandHandler<SendUserVerifiedNotificationCommand, Void> {
    private final ExternalNotificationSender externalNotificationSender;

    @Override
    public HandlerResult<Void> handle(SendUserVerifiedNotificationCommand command) {
        var user = command.getUser();
        externalNotificationSender.sendNotification(
                user.getEmail(),
                NotificationType.ACCOUNT_CONFIRMED,
                mapPayload(user)
        );
        return HandlerResult.success();
    }

    @Override
    public Class<? extends Command<Void>> commandClass() {
        return SendUserVerifiedNotificationCommand.class;
    }

    private Map<String, Object> mapPayload(User user) {
        return Map.of(
                "displayName", user.getDisplayName()
        );
    }
}
