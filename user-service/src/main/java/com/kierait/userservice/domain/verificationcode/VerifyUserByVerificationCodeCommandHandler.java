package com.kierait.userservice.domain.verificationcode;

import com.kierait.userservice.domain.exception.UserCannotBeVerifiedException;
import com.kierait.userservice.domain.exception.VerificationCodeNotFoundException;
import com.kierait.userservice.domain.user.AuthenticationUserDao;
import com.kierait.userservice.domain.user.UserDao;
import com.kierait.userservice.domain.user.model.User;
import com.kierait.userservice.domain.verificationcode.event.UserVerifiedEvent;
import com.kierait.userservice.domain.verificationcode.model.VerificationCode;
import com.kierait.util.annotations.Handler;
import com.kierait.util.event.EventPublisher;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import static com.kierait.userservice.domain.config.EventConstants.USER_VERIFIED_EVENT_DESTINATION;

@Handler
@RequiredArgsConstructor
class VerifyUserByVerificationCodeCommandHandler implements CommandHandler<VerifyUserByVerificationCodeCommand, User> {
    private final VerificationCodeDao verificationCodeDao;
    private final UserDao userDao;
    private final AuthenticationUserDao authenticationUserDao;
    private final EventPublisher eventPublisher;

    @Override
    @Transactional
    public HandlerResult<User> handle(VerifyUserByVerificationCodeCommand command) {
        return verificationCodeDao.findByCode(command.getCode())
                .filter(VerificationCode::isNotExpired)
                .map(this::verifyUser)
                .orElse(HandlerResult.error(new VerificationCodeNotFoundException()));
    }

    @Override
    public Class<? extends Command<User>> commandClass() {
        return VerifyUserByVerificationCodeCommand.class;
    }

    private HandlerResult<User> verifyUser(VerificationCode verificationCode) {
        var user = verificationCode.getUser();
        if (!user.canBeVerified()) {
            return HandlerResult.error(new UserCannotBeVerifiedException());
        }
        verifyUser(user);
        return HandlerResult.success(user);
    }

    private void verifyUser(User user) {
        user.verify();
        userDao.save(user);
        authenticationUserDao.verifyUser(user);
        notifyAboutConfirmedUser(user);
    }

    private void notifyAboutConfirmedUser(User user) {
        eventPublisher.publishEvent(
                USER_VERIFIED_EVENT_DESTINATION,
                new UserVerifiedEvent(user.getId())
        );
    }
}
