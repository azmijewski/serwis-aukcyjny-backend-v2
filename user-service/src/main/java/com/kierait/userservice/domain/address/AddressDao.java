package com.kierait.userservice.domain.address;

import com.kierait.userservice.domain.address.model.Address;

public interface AddressDao {
    Address save(Address address);
}
