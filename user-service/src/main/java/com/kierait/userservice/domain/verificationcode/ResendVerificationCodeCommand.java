package com.kierait.userservice.domain.verificationcode;

import com.kierait.util.handler.Command;
import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Value
public class ResendVerificationCodeCommand implements Command<Void> {
    @NotBlank
    @Email
    String email;
}
