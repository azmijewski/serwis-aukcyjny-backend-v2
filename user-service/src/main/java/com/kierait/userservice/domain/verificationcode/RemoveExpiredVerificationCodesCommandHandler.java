package com.kierait.userservice.domain.verificationcode;

import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.LocalDateTime;

@Handler
@RequiredArgsConstructor
@Slf4j
public class RemoveExpiredVerificationCodesCommandHandler implements CommandHandler<RemoveExpiredVerificationCodesCommand, Void> {
    private final VerificationCodeDao verificationCodeDao;
    private final Clock clock;

    @Override
    @Transactional
    public HandlerResult<Void> handle(RemoveExpiredVerificationCodesCommand command) {
        var expiredVerificationCodes = verificationCodeDao.findAllExpired();
        log.debug("Removing {} expired verification codes at {}", expiredVerificationCodes.size(), LocalDateTime.now(clock));
        verificationCodeDao.deleteAll(expiredVerificationCodes);
        return HandlerResult.success();
    }

    @Override
    public Class<? extends Command<Void>> commandClass() {
        return RemoveExpiredVerificationCodesCommand.class;
    }
}
