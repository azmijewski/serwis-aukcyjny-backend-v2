package com.kierait.userservice.domain.user;

import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

@Handler
@RequiredArgsConstructor
public class LogoutUserCommandHandler implements CommandHandler<LogoutUserCommand, Void> {
    private final AuthenticationUserDao authenticationUserDao;
    private final UserDao userDao;

    @Override
    @Transactional
    public HandlerResult<Void> handle(LogoutUserCommand command) {
        var user = userDao.getByGlobalId(command.getGlobalUserId());
        authenticationUserDao.logoutUser(user);
        return HandlerResult.success();
    }

    @Override
    public Class<? extends Command<Void>> commandClass() {
        return LogoutUserCommand.class;
    }
}
