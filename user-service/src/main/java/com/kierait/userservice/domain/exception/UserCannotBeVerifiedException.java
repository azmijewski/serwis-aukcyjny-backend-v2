package com.kierait.userservice.domain.exception;

import com.kierait.util.exception.ConflictException;

public class UserCannotBeVerifiedException extends ConflictException {
    public UserCannotBeVerifiedException() {
        super("Requested user is already confirmed", "USER_ALREADY_VERIFIED");
    }
}
