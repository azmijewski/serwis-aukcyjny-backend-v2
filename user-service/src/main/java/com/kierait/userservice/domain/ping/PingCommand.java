package com.kierait.userservice.domain.ping;

import com.kierait.util.handler.Command;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class PingCommand implements Command<String> {
    @NotBlank
    String message;
}
