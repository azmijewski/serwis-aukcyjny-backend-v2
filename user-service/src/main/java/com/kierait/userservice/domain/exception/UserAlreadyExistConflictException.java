package com.kierait.userservice.domain.exception;

import com.kierait.util.exception.ConflictException;

public class UserAlreadyExistConflictException extends ConflictException {
    public UserAlreadyExistConflictException(String email) {
        super("User with email: %s already exists".formatted(email), "USER_ALREADY_EXISTS");
    }
}
