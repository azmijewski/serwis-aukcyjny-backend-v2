package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.config.property.AppUrlProperties;
import com.kierait.userservice.domain.shared.ExternalNotificationSender;
import com.kierait.userservice.domain.shared.NotificationType;
import com.kierait.userservice.domain.verificationcode.model.VerificationCode;
import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Handler
@RequiredArgsConstructor
class SendUserRegisteredNotificationCommandHandler implements CommandHandler<SendUserRegisteredNotificationCommand, Void> {
    private final ExternalNotificationSender externalNotificationSender;
    private final AppUrlProperties appUrlProperties;

    @Override
    public HandlerResult<Void> handle(SendUserRegisteredNotificationCommand command) {
        var verificationCode = command.getVerificationCode();
        externalNotificationSender.sendNotification(
                verificationCode.getUser().getEmail(),
                NotificationType.AFTER_REGISTRATION,
                mapPayload(verificationCode)
        );
        return HandlerResult.success();
    }

    @Override
    public Class<? extends Command<Void>> commandClass() {
        return SendUserRegisteredNotificationCommand.class;
    }

    private Map<String, Object> mapPayload(VerificationCode verificationCode) {
        return Map.of(
                "displayName", verificationCode.getUser().getDisplayName(),
                "confirmUrl", "%s/%s".formatted(appUrlProperties.getConfirmAccountUrl(), verificationCode.getCode())
        );
    }
}
