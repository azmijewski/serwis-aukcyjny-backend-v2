package com.kierait.userservice.domain.verificationcode;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.userservice.domain.verificationcode.model.VerificationCode;

import java.util.Optional;
import java.util.Set;

public interface VerificationCodeDao {
    Optional<VerificationCode> findByCode(String code);
    Set<VerificationCode> findAllExpired();
    VerificationCode save(VerificationCode verificationCode);
    void deleteAll(Set<VerificationCode> verificationCodes);
    Set<VerificationCode> findAllByUser(User user);
}
