package com.kierait.userservice.domain.verificationcode;

import com.kierait.userservice.domain.user.SendUserRegisteredNotificationCommand;
import com.kierait.userservice.domain.user.UserDao;
import com.kierait.userservice.domain.user.model.User;
import com.kierait.userservice.domain.verificationcode.model.VerificationCode;
import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandDispatcher;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Handler
@RequiredArgsConstructor
public class ResendVerificationCodeCommandHandler implements CommandHandler<ResendVerificationCodeCommand, Void> {
    private final VerificationCodeDao verificationCodeDao;
    private final UserDao userDao;
    private final CommandDispatcher commandDispatcher;

    @Override
    @Transactional
    public HandlerResult<Void> handle(ResendVerificationCodeCommand command) {
        var user = userDao.findByEmail(command.getEmail());
        user.ifPresent(this::handleCodeForUser);
        return HandlerResult.success();
    }

    @Override
    public Class<? extends Command<Void>> commandClass() {
        return ResendVerificationCodeCommand.class;
    }

    private void handleCodeForUser(User user) {
        verificationCodeDao.findAllByUser(user)
                .stream()
                .filter(VerificationCode::isNotExpired)
                .findFirst()
                .ifPresentOrElse(
                    this::sendVerificationCode,
                        () -> createAndSendVerificationCode(user)
                );
    }

    private void createAndSendVerificationCode(User user) {
        Optional.ofNullable(
                commandDispatcher.dispatch(new CreateVerificationCodeForUserCommand(user)).getSuccess()
        ).ifPresent(this::sendVerificationCode);
    }

    private void sendVerificationCode(VerificationCode verificationCode) {
        commandDispatcher.dispatch(new SendUserRegisteredNotificationCommand(verificationCode));
    }
}
