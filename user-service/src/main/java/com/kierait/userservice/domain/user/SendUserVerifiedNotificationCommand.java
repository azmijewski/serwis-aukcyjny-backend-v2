package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.user.model.User;
import com.kierait.util.handler.Command;
import lombok.Value;

import javax.validation.constraints.NotNull;

@Value
public class SendUserVerifiedNotificationCommand implements Command<Void> {
    @NotNull
    User user;
}
