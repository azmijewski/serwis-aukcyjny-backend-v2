package com.kierait.userservice.domain.user;

import com.kierait.userservice.domain.user.model.User;

import java.util.Optional;

public interface AuthenticationUserDao {
    Optional<String> registerUser(User user, String password);
    void verifyUser(User user);
    void logoutUser(User user);
}
