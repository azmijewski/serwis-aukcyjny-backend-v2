create sequence address_id_sequence start 101 increment 1;

create table address
(
    id         BIGINT primary key default nextval('address_id_sequence'),
    city       varchar(255) not null,
    street     varchar(255) not null,
    number     varchar(50)  not null,
    apartment  varchar(50),
    post_code  varchar(9)   not null,
    type       varchar(10)  not null,
    created_at timestamp    not null,
    user_id    BIGINT       not null,

    foreign key (user_id) references users (id)
);
