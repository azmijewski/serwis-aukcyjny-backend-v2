create sequence verification_code_id_sequence start 101 increment 1;

create table verification_code
(
    id         BIGINT primary key default nextval('verification_code_id_sequence'),
    code       varchar(50) not null,
    created_at timestamp   not null,
    expiration_at timestamp   not null,
    user_id    BIGINT        not null,

    foreign key (user_id) references users (id)
);

create index if not exists verification_code__code_idx on verification_code (code);
