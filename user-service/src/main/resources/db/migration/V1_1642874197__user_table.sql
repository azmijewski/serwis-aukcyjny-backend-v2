CREATE SEQUENCE user_id_sequence START 101 INCREMENT 1;

create table users
(
    id           BIGINT PRIMARY KEY DEFAULT nextval('user_id_sequence'),
    global_id    VARCHAR(255) not null unique,
    email        VARCHAR(255) not null unique,
    pesel        VARCHAR(255) not null,
    first_name   VARCHAR(255) not null,
    last_name    VARCHAR(255) not null,
    phone_number VARCHAR(255) not null,
    created_at   TIMESTAMP    not null,
    status       VARCHAR(15)  not null,
    account_type VARCHAR(15)  not null
);

create index if not exists users__global_id_idx on users (global_id);
