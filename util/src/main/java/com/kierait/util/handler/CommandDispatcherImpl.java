package com.kierait.util.handler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("rawtypes")
class CommandDispatcherImpl implements CommandDispatcher {
    private final Map<Class<Command>, CommandHandler> handlersMap = new ConcurrentHashMap<>();

    @Override
    @SuppressWarnings("unchecked")
    public void registerHandler(CommandHandler handler) {
        if(handlersMap.containsKey(handler.commandClass())) {
            throw new IllegalArgumentException(String.format("Could not register commands with same commandClass %s", handler.commandClass()));
        }
        handlersMap.put(handler.commandClass(), handler);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <R> HandlerResult<R> dispatch(Command<R> command) {
        if (handlersMap.containsKey(command.getClass())) {
            return handlersMap.get(command.getClass()).handle(command);
        }
        throw new IllegalArgumentException(String.format("Could not find handler for command %s", command.getClass()));
    }

    @Override
    public <R> R dispatchAndGet(Command<R> command) {
        var result = dispatch(command);
        result.onErrorThrowException();
        return result.getSuccess();
    }
}
