package com.kierait.util.handler;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DispatchersConfig {
    @Bean
    CommandDispatcher commandDispatcher() {
       return new CommandDispatcherImpl();
    }

    @Bean
    QueryDispatcher queryDispatcher() {
        return new QueryDispatcherImpl();
    }
}
