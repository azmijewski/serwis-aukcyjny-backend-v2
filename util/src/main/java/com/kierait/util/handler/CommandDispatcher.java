package com.kierait.util.handler;

@SuppressWarnings("rawtypes")
public interface CommandDispatcher {
    void registerHandler(CommandHandler handler);
    <R> HandlerResult<R> dispatch(Command<R> command);
    <R> R dispatchAndGet(Command<R> command);
}
