package com.kierait.util.handler;

@SuppressWarnings("rawtypes")
public interface QueryDispatcher {
    void registerHandler(QueryHandler handler);

    <R> HandlerResult<R> dispatch(Query<R> query);
}
