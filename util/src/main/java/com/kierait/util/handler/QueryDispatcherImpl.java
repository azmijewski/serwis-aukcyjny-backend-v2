package com.kierait.util.handler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("rawtypes")
class QueryDispatcherImpl implements QueryDispatcher {
    private final Map<Class<Query>, QueryHandler> handlersMap = new ConcurrentHashMap<>();

    @SuppressWarnings("unchecked")
    public void registerHandler(QueryHandler handler) {
        if (handlersMap.containsKey(handler.queryClass())) {
            throw new IllegalArgumentException(String.format("Could not register query with same queryClass %s", handler.queryClass()));
        }
        handlersMap.put(handler.queryClass(), handler);

    }

    @SuppressWarnings("unchecked")
    public <R> HandlerResult<R> dispatch(Query<R> query) {
        if (handlersMap.containsKey(query.getClass())) {
            return handlersMap.get(query.getClass()).handle(query);
        }
        throw new IllegalArgumentException(String.format("Could not find handler for command %s", query.getClass()));
    }
}
