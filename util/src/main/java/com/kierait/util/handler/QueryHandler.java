package com.kierait.util.handler;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface QueryHandler<T extends Query<R>, R> {
    HandlerResult<R> handle(@Valid T query);

    @NotNull Class<? extends Query<R>> queryClass();
}
