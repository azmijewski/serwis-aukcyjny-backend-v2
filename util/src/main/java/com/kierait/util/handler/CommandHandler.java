package com.kierait.util.handler;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface CommandHandler<T extends Command<R>, R> {
    HandlerResult<R> handle(@Valid T command);

    @NotNull Class<? extends Command<R>> commandClass();
}
