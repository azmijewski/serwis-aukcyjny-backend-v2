package com.kierait.util.handler;

import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class DispatchersRegisterConfig {
    public DispatchersRegisterConfig(
            CommandDispatcher commandDispatcher,
            List<CommandHandler<?, ?>> commandHandlers,
            QueryDispatcher queryDispatcher,
            List<QueryHandler<?, ?>> queryHandlers
    ) {
        commandHandlers.forEach(commandDispatcher::registerHandler);
        queryHandlers.forEach(queryDispatcher::registerHandler);
    }
}
