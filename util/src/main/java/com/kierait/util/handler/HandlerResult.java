package com.kierait.util.handler;

import com.kierait.util.exception.BusinessException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public final class HandlerResult<T> {
    private final T success;
    private final BusinessException error;
    private final boolean isSuccess;

    public static <T> HandlerResult<T> success(T success) {
        return new HandlerResult<>(success, null, true);
    }

    public static HandlerResult<Void> success() {
        return new HandlerResult<>(null, null, true);
    }

    public static <T> HandlerResult<T> error(BusinessException error) {
        return new HandlerResult<>(null, error, false);
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void onErrorThrowException() {
        if (!isSuccess) {
            throw error;
        }
    }
}
