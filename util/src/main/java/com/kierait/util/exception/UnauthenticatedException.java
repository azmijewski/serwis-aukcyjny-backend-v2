package com.kierait.util.exception;

public class UnauthenticatedException extends BusinessException{
    public UnauthenticatedException() {
        super("Unauthenticated - access forbidden", "UNAUTHENTICATED", 401);
    }
}
