package com.kierait.util.exception;

public class InheritedBusinessException extends BusinessException{
    public InheritedBusinessException(BusinessException businessException) {
        super(businessException.getMessage(), businessException.getErrorCode(), businessException.getResponseCode());
    }
}
