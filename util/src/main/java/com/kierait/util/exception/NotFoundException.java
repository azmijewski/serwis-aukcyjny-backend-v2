package com.kierait.util.exception;

public abstract class NotFoundException extends BusinessException {
    public NotFoundException(String message, String errorCode) {
        super(message, errorCode, 404);
    }
}
