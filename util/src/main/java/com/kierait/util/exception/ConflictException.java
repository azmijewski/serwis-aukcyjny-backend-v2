package com.kierait.util.exception;

public abstract class ConflictException extends BusinessException {
    public ConflictException(String message, String errorCode) {
        super(message, errorCode, 409);
    }
}
