package com.kierait.util.exception;

import lombok.Getter;

@Getter
public abstract class BusinessException extends RuntimeException{
    protected String errorCode;
    protected int responseCode;

    public BusinessException(String message, String errorCode, int responseCode) {
        super(message);
        this.errorCode = errorCode;
        this.responseCode = responseCode;
    }
}
