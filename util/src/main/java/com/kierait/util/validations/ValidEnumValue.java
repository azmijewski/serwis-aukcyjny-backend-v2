package com.kierait.util.validations;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Constraint(validatedBy = ValidEnumValueValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidEnumValue {
    String message() default "Invalid enum value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    Class<? extends Enum<?>> enumClass();
}
