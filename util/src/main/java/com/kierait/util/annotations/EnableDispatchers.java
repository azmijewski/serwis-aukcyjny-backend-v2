package com.kierait.util.annotations;

import com.kierait.util.handler.DispatchersConfig;
import com.kierait.util.handler.DispatchersRegisterConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({DispatchersConfig.class, DispatchersRegisterConfig.class})
public @interface EnableDispatchers {
}
