package com.kierait.util.api;

import lombok.Builder;
import lombok.Value;

import java.time.Instant;
import java.util.UUID;

@Value
@Builder
public class ApiErrorResponse {
    Instant timestamp = Instant.now();
    String code;
    String message;
    UUID uuid = UUID.randomUUID();

    public ApiErrorResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
