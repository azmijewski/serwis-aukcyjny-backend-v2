package com.kierait.util.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
public abstract class ExceptionApi {
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrorResponse onMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        var errorCode = exception.getBindingResult()
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .findFirst()
                .orElse("VALIDATION_ERROR");
        return new ApiErrorResponse(errorCode, "Validation failed");
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiErrorResponse onThrowable(Throwable throwable) {
      log.error("API ERROR", throwable);
      return new ApiErrorResponse("INTERNAL_ERROR", "Internal server error");
    }
}
