package com.kierait.util.api;

import com.kierait.util.handler.Query;
import com.kierait.util.handler.QueryDispatcher;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;

import java.util.function.Function;

@RequiredArgsConstructor
public class ApiQueryDispatcher {
    private final QueryDispatcher queryDispatcher;


    public <T> ResponseEntity<?> dispatch(Query<T> query) {
        var handlerResult =  queryDispatcher.dispatch(query);
        return ResponseEntityHandlerResultResolver.resolveResponseEntity(handlerResult, result -> result);
    }

    public <T, R> ResponseEntity<?> dispatch(Query<T> query, Function<T, R> transformer) {
        var handlerResult =  queryDispatcher.dispatch(query);
        return ResponseEntityHandlerResultResolver.resolveResponseEntity(handlerResult, transformer);
    }
}
