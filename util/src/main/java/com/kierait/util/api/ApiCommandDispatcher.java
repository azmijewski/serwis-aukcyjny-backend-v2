package com.kierait.util.api;

import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandDispatcher;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;

import java.util.function.Function;

@RequiredArgsConstructor
public class ApiCommandDispatcher {
    private final CommandDispatcher commandDispatcher;

    public <T> ResponseEntity<?> dispatch(Command<T> command) {
        var handlerResult =  commandDispatcher.dispatch(command);
        return ResponseEntityHandlerResultResolver.resolveResponseEntity(handlerResult, result -> result);
    }

    public <T, R> ResponseEntity<?> dispatch(Command<T> command, Function<T, R> transformer) {
        var handlerResult =  commandDispatcher.dispatch(command);
        return ResponseEntityHandlerResultResolver.resolveResponseEntity(handlerResult, transformer);
    }
}
