package com.kierait.util.api;

import com.kierait.util.exception.BusinessException;
import com.kierait.util.handler.HandlerResult;
import org.springframework.http.ResponseEntity;

import javax.validation.constraints.NotNull;
import java.util.function.Function;

public final class ResponseEntityHandlerResultResolver {
    public static <T, R> ResponseEntity<?> resolveResponseEntity(@NotNull HandlerResult<T> handlerResult, Function<T, R> transformer) {
        if (handlerResult.isSuccess()) {
            var success = handlerResult.getSuccess();
            return ResponseEntity.ok(transformer.apply(success));
        }
        var businessException = handlerResult.getError();
        return ResponseEntity
                .status(businessException.getResponseCode())
                .body(prepareApiErrorResponse(businessException));
    }


    private static ApiErrorResponse prepareApiErrorResponse(BusinessException businessException) {
        return ApiErrorResponse.builder()
                .code(businessException.getErrorCode())
                .message(businessException.getMessage())
                .build();
    }
}
