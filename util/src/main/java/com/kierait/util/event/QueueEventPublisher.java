package com.kierait.util.event;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.function.StreamBridge;

@RequiredArgsConstructor
public class QueueEventPublisher implements EventPublisher {
    private final StreamBridge streamBridge;

    @Override
    public <T> void publishEvent(String destination, T event) {
        streamBridge.send(destination, event);
    }
}
