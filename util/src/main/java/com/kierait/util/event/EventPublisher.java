package com.kierait.util.event;

public interface EventPublisher {
    <T> void publishEvent(String destination, T event);
}
