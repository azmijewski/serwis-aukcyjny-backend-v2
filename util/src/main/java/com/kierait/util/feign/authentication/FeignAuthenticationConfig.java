package com.kierait.util.feign.authentication;

import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@ConfigurationPropertiesScan
public class FeignAuthenticationConfig {

    @Bean
    TokenHolder tokenHolder(TokenExchange tokenExchange) {
        return new TokenHolder(tokenExchange);
    }

    @Bean
    TokenExchange tokenExchange(AuthenticationProperties authenticationProperties, RestTemplate oauth2RestTemplate) {
        return new TokenExchange(authenticationProperties, oauth2RestTemplate);
    }

    @Bean
    RestTemplate oauth2RestTemplate() {
        return new RestTemplate();
    }
}
