package com.kierait.util.feign.authentication;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "app.feign.oauth2")
public class AuthenticationProperties {
    private boolean enabled;
    private String tokenUrl;
    private String clientId;
    private String clientSecret;
}
