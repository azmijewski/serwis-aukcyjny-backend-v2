package com.kierait.util.feign.authentication;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class AuthenticationRequestInterceptor implements RequestInterceptor {
    private final TokenHolder tokenHolder;

    @Override
    public void apply(RequestTemplate template) {
        template.header("Authorization", "Bearer %s".formatted(tokenHolder.getToken()));
    }
}
