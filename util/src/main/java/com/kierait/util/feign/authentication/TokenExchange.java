package com.kierait.util.feign.authentication;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
public class TokenExchange {
    private final AuthenticationProperties authenticationProperties;
    private final RestTemplate restTemplate;

    public TokenResponse getTokenResponse() {
        HttpEntity<MultiValueMap<String, String>> formEntity = createRequestBody();
        return restTemplate.exchange(authenticationProperties.getTokenUrl(), HttpMethod.POST, formEntity, TokenResponse.class).getBody();
    }

    private HttpEntity<MultiValueMap<String, String>> createRequestBody() {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("client_id", authenticationProperties.getClientId());
        body.add("client_secret", authenticationProperties.getClientSecret());
        body.add("grant_type", "client_credentials");
        return new HttpEntity<>(body, createHttpHeaders());
    }

    private HttpHeaders createHttpHeaders() {
        var headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        return headers;
    }
}
