package com.kierait.util.feign;

import com.kierait.util.feign.authentication.AuthenticationRequestInterceptor;
import com.kierait.util.feign.authentication.TokenHolder;
import feign.RequestInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

public class GlobalFeignConfig {

    @Bean
    public RequestInterceptor requestInterceptor(TokenHolder tokenHolder) {
        return new AuthenticationRequestInterceptor(tokenHolder);
    }
}
