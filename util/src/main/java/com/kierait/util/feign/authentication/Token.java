package com.kierait.util.feign.authentication;

import com.kierait.util.clock.DefaultClock;
import lombok.Value;

import java.time.Clock;
import java.time.Instant;

@Value
public class Token {
    private static final Clock CLOCK = DefaultClock.getInstance();
    private static final int TOKEN_EXPIRATION_TIME_MARGIN_SECONDS = 15;

    String token;
    Instant expiresAt;

    public boolean isExpired() {
        return CLOCK.instant().plusSeconds(TOKEN_EXPIRATION_TIME_MARGIN_SECONDS).isAfter(expiresAt);
    }
}
