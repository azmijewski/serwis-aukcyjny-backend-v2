package com.kierait.util.feign.authentication;

import com.kierait.util.clock.DefaultClock;
import lombok.RequiredArgsConstructor;

import java.time.Clock;

@RequiredArgsConstructor
public class TokenHolder {
    private static final Clock CLOCK = DefaultClock.getInstance();

    private Token token;

    private final TokenExchange tokenExchange;

    public String getToken() {
        if (token == null || token.isExpired()) {
            fetchAndSaveToken();
        }
        return token.getToken();
    }

    private void fetchAndSaveToken() {
        var tokenResponse = tokenExchange.getTokenResponse();
        token = new Token(tokenResponse.getAccessToken(), CLOCK.instant().plusSeconds(tokenResponse.getExpiresIn()));
    }

}
