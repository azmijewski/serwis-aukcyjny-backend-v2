package com.kierait.util.security;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.NONE)
final class KeycloakJwtConstants {
    static final String EMAIL = "email";
    static final String GLOBAL_USER_ID = "sub";
    static final String REALM_ACCESS = "realm_access";
    static final String ROLES = "roles";
}
