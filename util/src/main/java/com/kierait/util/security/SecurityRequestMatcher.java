package com.kierait.util.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.Arrays;
import java.util.List;

public class SecurityRequestMatcher {
    private final List<HttpMethod> httpMethods;
    private final List<String> paths;

    public SecurityRequestMatcher(String... paths) {
        this.paths = Arrays.stream(paths).toList();
        this.httpMethods = null;
    }

    public SecurityRequestMatcher(List<HttpMethod> httpMethods, String... paths) {
        this.httpMethods = httpMethods;
        this.paths = Arrays.stream(paths).toList();
    }

    public List<AntPathRequestMatcher> toAntPathMatcher() {
        if (httpMethods == null) {
            return paths.stream()
                    .map(AntPathRequestMatcher::new)
                    .toList();
        }
        return this.paths.stream()
                .flatMap(path -> this.httpMethods.stream()
                        .map(httpMethod -> new AntPathRequestMatcher(path, httpMethod.name()))
                ).toList();
    }
}
