package com.kierait.util.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecurityProviderConfiguration {
    @Bean
    SecurityContextProvider securityContextProvider() {
        return new JwtSecurityContextProvider();
    }
}
