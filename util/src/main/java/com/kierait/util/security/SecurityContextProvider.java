package com.kierait.util.security;

public interface SecurityContextProvider {
    SecurityContext getCurrentContext();
}
