package com.kierait.util.security;

import com.kierait.util.exception.UnauthenticatedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

import java.util.List;
import java.util.Optional;

import static com.kierait.util.security.KeycloakJwtConstants.EMAIL;
import static com.kierait.util.security.KeycloakJwtConstants.GLOBAL_USER_ID;
import static com.kierait.util.security.KeycloakJwtConstants.REALM_ACCESS;
import static com.kierait.util.security.KeycloakJwtConstants.ROLES;

class JwtSecurityContextProvider implements SecurityContextProvider {

    @Override
    public SecurityContext getCurrentContext() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .filter(authentication -> authentication instanceof JwtAuthenticationToken)
                .map(token -> map((Jwt) token.getPrincipal()))
                .orElseThrow(UnauthenticatedException::new);
    }

    private SecurityContext map(Jwt jwt) {
        return new SecurityContext(
                jwt.getClaimAsString(GLOBAL_USER_ID),
                jwt.getClaimAsString(EMAIL),
                extractRoles(jwt)
        );
    }

    @SuppressWarnings("unchecked")
    private List<String> extractRoles(Jwt jwt) {
        var realmAccessClaims = jwt.getClaimAsMap(REALM_ACCESS);
        return (List<String>) realmAccessClaims.get(ROLES);
    }
}

