package com.kierait.util.security;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.kierait.util.security.KeycloakJwtConstants.REALM_ACCESS;
import static com.kierait.util.security.KeycloakJwtConstants.ROLES;

public class KeycloakGrantedAuthoritiesConverter implements Converter<Jwt, Collection<GrantedAuthority>> {

    @Override
    @SuppressWarnings("unchecked")
    public List<GrantedAuthority> convert(Jwt source) {
        var realmAccess = source.getClaimAsMap(REALM_ACCESS);
        return ((List<String>) realmAccess.get(ROLES)).stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}
