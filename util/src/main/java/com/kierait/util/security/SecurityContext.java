package com.kierait.util.security;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
public class SecurityContext {
    private final String id;
    private final String email;
    private final List<String> roles;
}
