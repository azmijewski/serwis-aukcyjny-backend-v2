package com.kierait.util.clock;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.Clock;

@NoArgsConstructor(access = AccessLevel.NONE)
public final class DefaultClock {
    private static final Clock INSTANCE = Clock.systemDefaultZone();

    public static Clock getInstance() {
        return INSTANCE;
    }
}
