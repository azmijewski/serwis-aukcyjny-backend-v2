package com.kierait.notificationservice.api.service.ping

import com.kierait.notificationservice.BaseIntegrationSpec
import com.kierait.notificationservice.api.service.ping.PingRequest
import com.kierait.notificationservice.api.service.ping.PingResponse
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post

class PingApiTest extends BaseIntegrationSpec {
    private static final String BASE_URL = "/api/ping"
    private static final String MESSAGE = "Test"

    @WithMockUser
    def "should ping"() {
        when:
            def response = mockMvc.perform(pingRequest()).andReturn().response
        then: "response is 200 OK"
            response.status == HttpStatus.OK.value()
        and: "response body is ok"
            def responseContent = objectMapper.readValue(response.contentAsString, PingResponse.class)
            responseContent.message == MESSAGE + MESSAGE
    }

    private MockHttpServletRequestBuilder pingRequest(String message = MESSAGE) {
        post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new PingRequest(message)))
                .with(csrf())
    }
}
