package com.kierait.notificationservice.api.service.message

import com.kierait.notificationservice.BaseIntegrationSpec
import com.kierait.notificationservice.domain.message.MessageDao
import com.kierait.notificationservice.domain.message.model.Message
import com.kierait.notificationservice.domain.message.model.MessageStatus
import com.kierait.notificationservice.domain.message.model.MessageType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder

import java.time.LocalDateTime

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

class MessageApiTest extends BaseIntegrationSpec {
    private static final BASE_URL = "/messages"
    private static final RECIPIENT = "test@test.pl"
    private static final ANOTHER_RECIPIENT = "another_test@test.pl"
    private static final PAYLOAD = "{}"
    private static final MESSAGE_TYPE = MessageType.AFTER_REGISTRATION
    private static final ANOTHER_MESSAGE_TYPE = MessageType.ACCOUNT_CONFIRMED
    private static final SCHEDULE_MESSAGE_REQUEST = new ScheduleMessageRequest(RECIPIENT, PAYLOAD, MESSAGE_TYPE.name())
    private static final CANCEL_MESSAGES_FOR_RECIPIENT_HANDLER = new CancelMessagesRequest(RECIPIENT)

    @Autowired
    private MessageDao messageDao

    def "should schedule message"() {
        when:
        def response = mockMvc.perform(scheduleMessage()).andReturn().response
        then: "response is 200 ok"
        response.status == HttpStatus.OK.value()
        and: "message is created"
        transactionTemplate.executeWithoutResult({
            def message = messageDao.findAll()[0]
            assert message.type == MESSAGE_TYPE
            assert message.recipient == RECIPIENT
            assert message.payload == PAYLOAD
            assert message.status == MessageStatus.CREATED
        })

    }

    def "should not schedule message when invalid messageType"() {
        given:
        def request = new ScheduleMessageRequest(RECIPIENT, PAYLOAD, "invalid_type")
        when:
        def response = mockMvc.perform(scheduleMessage(request)).andReturn().response
        then: "response is 400 bad request"
        response.status == HttpStatus.BAD_REQUEST.value()
        and: "message is not created"
        transactionTemplate.executeWithoutResult({
            assert messageDao.count() == 0
        })
    }

    def "should cancel messages for recipient"() {
        given:
        createMessage(RECIPIENT, PAYLOAD, MESSAGE_TYPE, MessageStatus.SENT)
        createMessage(RECIPIENT, PAYLOAD, ANOTHER_MESSAGE_TYPE)
        createMessage(ANOTHER_RECIPIENT, PAYLOAD, MESSAGE_TYPE)
        when:
        def response = mockMvc.perform(cancelMessagesForRecipient()).andReturn().response
        then: "response is 200 ok"
        response.status == HttpStatus.OK.value()
        and: "valid message is cancelled"
        def  messages = transactionTemplate.execute({
            messageDao.findAll()
        })
        and: "already sent message is not cancelled"
        def alreadySentMessage = messages.find { it.type == MESSAGE_TYPE && it.recipient == RECIPIENT } as Message
        alreadySentMessage.status == MessageStatus.SENT
        and: "message for another recipient is not cancelled"
        def messageForAnotherRecipient = messages.find { it.recipient == ANOTHER_RECIPIENT } as Message
        messageForAnotherRecipient.status == MessageStatus.CREATED
    }

    def createMessage(String recipient = RECIPIENT, String payload = PAYLOAD, MessageType messageType = MESSAGE_TYPE, MessageStatus messageStatus = MessageStatus.CREATED) {
        def message = new Message(
                null,
                recipient,
                LocalDateTime.now(),
                messageStatus,
                payload,
                messageType,
                0,
                null
        )
        transactionTemplate.execute({
            messageDao.save(message)
        })
    }

    private MockHttpServletRequestBuilder scheduleMessage(ScheduleMessageRequest scheduleMessageRequest = SCHEDULE_MESSAGE_REQUEST) {
        post(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(scheduleMessageRequest))
                .with(csrf())
    }

    private MockHttpServletRequestBuilder cancelMessagesForRecipient(CancelMessagesRequest cancelMessagesForRecipientRequest = CANCEL_MESSAGES_FOR_RECIPIENT_HANDLER) {
        put(BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(cancelMessagesForRecipientRequest))
                .with(csrf())
    }
}
