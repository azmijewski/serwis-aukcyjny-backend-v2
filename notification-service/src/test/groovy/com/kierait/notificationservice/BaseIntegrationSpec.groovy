package com.kierait.notificationservice

import com.fasterxml.jackson.databind.ObjectMapper
import com.kierait.notificationservice.domain.message.EmailSender
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.support.TransactionTemplate
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest
@ActiveProfiles(["test", "disabled-scheduling"])
@AutoConfigureMockMvc
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/sql/clean-after-test.sql")
class BaseIntegrationSpec extends Specification {
    protected static final MAIL_WITH_FAIL_RECIPIENT = "fail@test.com"

    @Autowired
    protected ObjectMapper objectMapper

    @Autowired
    protected MockMvc mockMvc

    @Autowired
    protected TransactionTemplate transactionTemplate

    @Shared
    static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres")
            .withDatabaseName("serwis-aukcyjny")

    @SpringBean
    EmailSender emailSender = new EmailSender() {

        @Override
        boolean sendMessage(String recipient, String content, String subject) {
            return MAIL_WITH_FAIL_RECIPIENT != recipient
        }
    }


    static {
        postgreSQLContainer.start()
        System.setProperty("test.container.db.userName", postgreSQLContainer.getUsername())
        System.setProperty("test.container.db.password", postgreSQLContainer.getUsername())
        System.setProperty("test.container.db.jdbcUrl", postgreSQLContainer.getJdbcUrl())
    }
}
