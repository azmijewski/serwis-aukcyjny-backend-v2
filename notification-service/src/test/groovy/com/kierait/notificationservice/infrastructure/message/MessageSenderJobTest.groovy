package com.kierait.notificationservice.infrastructure.message

import com.kierait.notificationservice.BaseIntegrationSpec
import com.kierait.notificationservice.domain.message.MessageDao
import com.kierait.notificationservice.domain.message.model.Message
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Subject

import java.time.LocalDateTime

class MessageSenderJobTest extends BaseIntegrationSpec {

    @Subject
    @Autowired
    private MessageSenderJob messageSenderJob

    @Autowired
    private MessageDao messageRepository

    def "should send messages"() {
        given:
            def messageForSuccessSentId = createMessage("success@test.pl", com.kierait.notificationservice.domain.message.model.MessageStatus.CREATED).id
            def messageForFailId = createMessage(MAIL_WITH_FAIL_RECIPIENT, com.kierait.notificationservice.domain.message.model.MessageStatus.CREATED).id
            def messageForFailedAfterFailId = createMessage(MAIL_WITH_FAIL_RECIPIENT, com.kierait.notificationservice.domain.message.model.MessageStatus.CREATED, 2).id
        when:
            messageSenderJob.sendMessages()
        then:
            def messages = transactionTemplate.execute({
                messageRepository.findAll()
            })
            messages.find { it.id == messageForSuccessSentId }.status == com.kierait.notificationservice.domain.message.model.MessageStatus.SENT
            messages.find { it.id == messageForFailId }.status == com.kierait.notificationservice.domain.message.model.MessageStatus.CREATED
            messages.find { it.id == messageForFailedAfterFailId }.status == com.kierait.notificationservice.domain.message.model.MessageStatus.FAILED
    }

    def createMessage(String recipient, com.kierait.notificationservice.domain.message.model.MessageStatus messageStatus, int attempts = 0, String payload = "{\"displayName\":\"Test Testowy\"}") {
        def message = new Message(
                null,
                recipient,
                LocalDateTime.now(),
                messageStatus,
                payload,
                com.kierait.notificationservice.domain.message.model.MessageType.ACCOUNT_CONFIRMED,
                attempts,
                null
        )
        transactionTemplate.execute({
            messageRepository.save(message)
        })
    }
}
