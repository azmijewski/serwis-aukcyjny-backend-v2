CREATE SEQUENCE message_config_id_sequence START 101 INCREMENT 1;

create table message_config
(
    id           BIGINT PRIMARY KEY DEFAULT nextval('message_config_id_sequence'),
    content      text        not null,
    type varchar(50) not null,
    subject      varchar(100) not null,
    start_date   timestamp   not null,
    end_date     timestamp   not null
);

create index if not exists message_config__start_date_idx on message_config (start_date);
create index if not exists message_config__end_date_idx on message_config (end_date);
