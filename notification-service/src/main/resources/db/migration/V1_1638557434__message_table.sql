CREATE SEQUENCE message_id_sequence START 101 INCREMENT 1;

create table message
(
    id             BIGINT PRIMARY KEY DEFAULT nextval('message_id_sequence'),
    payload        text,
    type   varchar(50) not null,
    attempts       int                default 0,
    status varchar(15) not null,
    created_at     timestamp,
    recipient      varchar(75) not null,
    completed_at   timestamp
);
