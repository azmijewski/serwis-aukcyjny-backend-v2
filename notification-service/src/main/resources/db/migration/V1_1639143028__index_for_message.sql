create index if not exists message__recipient_idx on message (recipient);
create index if not exists message__attempts_idx on message (attempts);
