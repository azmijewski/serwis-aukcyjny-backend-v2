package com.kierait.notificationservice.domain.message;

import com.kierait.notificationservice.domain.message.model.Message;
import com.kierait.notificationservice.domain.message.model.MessageType;
import com.kierait.util.handler.Command;
import lombok.Value;

@Value
public class ScheduleMessageCommand implements Command<Message> {
    String recipient;
    String payload;
    MessageType type;
}
