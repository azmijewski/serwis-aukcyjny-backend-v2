package com.kierait.notificationservice.domain.message;

import com.kierait.notificationservice.domain.exception.ContentPreparationException;
import com.kierait.notificationservice.domain.message.model.Message;
import com.kierait.notificationservice.domain.messageconfig.MessageConfigDao;
import com.kierait.notificationservice.domain.messageconfig.model.MessageConfig;
import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.LocalDateTime;

@Handler
@RequiredArgsConstructor
@Slf4j
class SendMessagesCommandHandler implements CommandHandler<SendMessagesCommand, Void> {
    private static final Integer MAX_ATTEMPTS = 3;

    private final MessageDao messageDao;
    private final MessageConfigDao messageConfigDao;
    private final ContentCreator contentCreator;
    private final EmailSender emailSender;
    private final Clock clock;

    @Override
    @Transactional
    public HandlerResult<Void> handle(SendMessagesCommand command) {
        var messagesToSend = messageDao.findAllToSend(MAX_ATTEMPTS);
        log.debug("Sending {} messages at {}", messagesToSend.size(), LocalDateTime.now(clock));
        messagesToSend.forEach(this::sendMessage);
        messageDao.saveAll(messagesToSend);
        return HandlerResult.success();
    }

    @Override
    public Class<? extends Command<Void>> commandClass() {
        return SendMessagesCommand.class;
    }

    private void sendMessage(Message message) {
        messageConfigDao.findCurrentConfigForType(message.getType()).ifPresentOrElse(
                messageConfig -> sendMessage(message, messageConfig),
                () -> log.error("Could not find current template for messageType: {}", message.getType())
        );
    }

    private void sendMessage(Message message, MessageConfig messageConfig) {
        try {
            var content = contentCreator.prepareContent(messageConfig.getContent(), message.getPayload());
            if (emailSender.sendMessage(message.getRecipient(), content, messageConfig.getSubject())) {
                message.complete();
            } else {
                log.warn("Could not send message: {}", message.getId());
                message.fail(MAX_ATTEMPTS);
            }
        } catch (ContentPreparationException ex) {
            log.error("Error while preparing content for message with id: {}", message.getId());
            message.fail(MAX_ATTEMPTS);
        }
    }
}
