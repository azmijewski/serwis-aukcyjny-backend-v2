package com.kierait.notificationservice.domain.messageconfig;

import com.kierait.notificationservice.domain.message.model.MessageType;
import com.kierait.notificationservice.domain.messageconfig.model.MessageConfig;

import java.util.Optional;

public interface MessageConfigDao {
    Optional<MessageConfig> findCurrentConfigForType(MessageType type);
}
