package com.kierait.notificationservice.domain.message.model;

public enum MessageStatus {
    CREATED,
    SENT,
    CANCELLED,
    FAILED,
}
