package com.kierait.notificationservice.domain.message;

import com.kierait.notificationservice.domain.exception.ContentPreparationException;

public interface ContentCreator {
    String prepareContent(String content, String payload) throws ContentPreparationException;
}
