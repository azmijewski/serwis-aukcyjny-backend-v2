package com.kierait.notificationservice.domain.ping;

import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import org.springframework.stereotype.Service;

@Handler
class PingCommandHandler implements CommandHandler<PingCommand, String> {
    @Override
    public HandlerResult<String> handle(PingCommand command) {
        String doubledMessage = command.getMessage().repeat(2);
        return HandlerResult.success(doubledMessage);
    }

    @Override
    public Class<? extends Command<String>> commandClass() {
        return PingCommand.class;
    }
}
