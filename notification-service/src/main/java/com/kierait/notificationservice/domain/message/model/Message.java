package com.kierait.notificationservice.domain.message.model;

import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@Builder
public class Message {
    private Long id;
    private String recipient;
    @Builder.Default
    private LocalDateTime createdAt = LocalDateTime.now();
    @Builder.Default
    private MessageStatus status = MessageStatus.CREATED;
    private String payload;
    private MessageType type;
    private int attempts;
    private LocalDateTime completedAt;

    public void complete() {
        this.status = MessageStatus.SENT;
        this.completedAt = LocalDateTime.now();
    }

    public void fail(int maxAttempts) {
        this.attempts++;
        if (this.attempts >= maxAttempts) {
            this.status = MessageStatus.FAILED;
        }
    }

    public void cancel() {
        this.status = MessageStatus.CANCELLED;
    }
}
