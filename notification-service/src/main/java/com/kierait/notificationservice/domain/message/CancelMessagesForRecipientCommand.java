package com.kierait.notificationservice.domain.message;

import com.kierait.util.handler.Command;
import lombok.Value;

@Value
public class CancelMessagesForRecipientCommand implements Command<Integer> {
    String recipient;
}
