package com.kierait.notificationservice.domain.messageconfig.model;

import com.kierait.notificationservice.domain.message.model.MessageType;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@EqualsAndHashCode
@Builder
public class MessageConfig {
    private Long id;
    private MessageType type;
    private String content;
    private String subject;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
}
