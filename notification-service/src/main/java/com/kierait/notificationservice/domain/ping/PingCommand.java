package com.kierait.notificationservice.domain.ping;

import com.kierait.util.handler.Command;
import lombok.Value;

@Value
public class PingCommand implements Command<String> {
    String message;
}
