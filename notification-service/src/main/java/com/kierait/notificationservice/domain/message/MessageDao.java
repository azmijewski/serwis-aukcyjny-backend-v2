package com.kierait.notificationservice.domain.message;

import com.kierait.notificationservice.domain.message.model.Message;
import com.kierait.notificationservice.domain.message.model.MessageStatus;

import java.util.Set;

public interface MessageDao {
    Set<Message> findAllToSend(int maxAttempts);
    Set<Message> findAllByRecipientAndStatusWithLock(String recipient, MessageStatus status);
    Message save(Message message);
    Set<Message> saveAll(Set<Message> messages);
    Set<Message> findAll();
    long count();
}
