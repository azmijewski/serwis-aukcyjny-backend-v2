package com.kierait.notificationservice.domain.message.model;

public enum MessageType {
    AFTER_REGISTRATION,
    ACCOUNT_CONFIRMED
}
