package com.kierait.notificationservice.domain.message;

import com.kierait.notificationservice.domain.message.model.Message;

public interface EmailSender {
    boolean sendMessage(String recipient, String content, String subject);
}
