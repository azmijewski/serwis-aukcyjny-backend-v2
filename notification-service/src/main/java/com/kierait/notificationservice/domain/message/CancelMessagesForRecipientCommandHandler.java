package com.kierait.notificationservice.domain.message;

import com.kierait.notificationservice.domain.message.model.Message;
import com.kierait.notificationservice.domain.message.model.MessageStatus;
import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Handler
@RequiredArgsConstructor
public class CancelMessagesForRecipientCommandHandler implements CommandHandler<CancelMessagesForRecipientCommand, Integer> {
    private final MessageDao messageDao;

    @Override
    @Transactional
    public HandlerResult<Integer> handle(CancelMessagesForRecipientCommand command) {
        var messagesToCancel = messageDao.findAllByRecipientAndStatusWithLock(command.getRecipient(), MessageStatus.CREATED);
        messagesToCancel.forEach(Message::cancel);
        messageDao.saveAll(messagesToCancel);
        return HandlerResult.success(messagesToCancel.size());
    }

    @Override
    public Class<? extends Command<Integer>> commandClass() {
        return CancelMessagesForRecipientCommand.class;
    }
}
