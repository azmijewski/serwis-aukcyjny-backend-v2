package com.kierait.notificationservice.domain.message;

import com.kierait.notificationservice.domain.message.model.Message;
import com.kierait.util.annotations.Handler;
import com.kierait.util.handler.Command;
import com.kierait.util.handler.CommandHandler;
import com.kierait.util.handler.HandlerResult;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

@Handler
@RequiredArgsConstructor
class ScheduleMessageCommandHandler implements CommandHandler<ScheduleMessageCommand, Message> {
    private final MessageDao messageDao;

    @Override
    @Transactional
    public HandlerResult<Message> handle(ScheduleMessageCommand command) {
        var message = buildMessage(command);
        var createdMessage = messageDao.save(message);
        return HandlerResult.success(createdMessage);
    }

    @Override
    public Class<? extends Command<Message>> commandClass() {
        return ScheduleMessageCommand.class;
    }

    private Message buildMessage(ScheduleMessageCommand command) {
        return Message.builder()
                .recipient(command.getRecipient())
                .payload(command.getPayload())
                .type(command.getType())
                .build();
    }
}
