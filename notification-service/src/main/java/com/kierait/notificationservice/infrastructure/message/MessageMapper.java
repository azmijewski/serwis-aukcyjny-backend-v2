package com.kierait.notificationservice.infrastructure.message;

import com.kierait.notificationservice.domain.message.model.Message;
import com.kierait.util.mapper.MapperConfiguration;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfiguration.class)
interface MessageMapper {
    Message map(MessageEntity messageEntity);
    MessageEntity map(Message message);
    MessageStatus mapStatus(com.kierait.notificationservice.domain.message.model.MessageStatus status);
}
