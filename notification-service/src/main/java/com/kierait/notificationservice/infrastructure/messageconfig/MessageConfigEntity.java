package com.kierait.notificationservice.infrastructure.messageconfig;

import com.kierait.notificationservice.infrastructure.message.MessageType;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@Entity
@Table(name = "message_config")
class MessageConfigEntity {
    private static final String SEQUENCE_NAME = "message_config_id_sequence";

    @Id
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    private Long id;

    @Enumerated(EnumType.STRING)
    private MessageType type;

    private String content;

    private String subject;

    private LocalDateTime startDate;

    private LocalDateTime endDate;
}
