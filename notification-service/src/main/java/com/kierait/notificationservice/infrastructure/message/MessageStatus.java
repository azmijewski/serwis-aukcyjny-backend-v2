package com.kierait.notificationservice.infrastructure.message;

enum MessageStatus {
    CREATED,
    SENT,
    CANCELLED,
    FAILED,
}
