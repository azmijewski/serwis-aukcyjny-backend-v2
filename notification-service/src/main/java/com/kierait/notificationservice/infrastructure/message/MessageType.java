package com.kierait.notificationservice.infrastructure.message;

public enum MessageType {
    AFTER_REGISTRATION,
    ACCOUNT_CONFIRMED
}
