package com.kierait.notificationservice.infrastructure.message;

import com.kierait.notificationservice.domain.message.MessageDao;
import com.kierait.notificationservice.domain.message.model.Message;
import com.kierait.notificationservice.domain.message.model.MessageStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@Transactional(propagation = Propagation.MANDATORY)
@RequiredArgsConstructor
class MessageDaoImpl implements MessageDao {
    private final MessageEntityRepository messageEntityRepository;
    private final MessageMapper messageMapper;

    @Override
    public Set<Message> findAllToSend(int maxAttempts) {
        return messageEntityRepository.findAllByStatusAndAndAttemptsLessThanEqual(
                        com.kierait.notificationservice.infrastructure.message.MessageStatus.CREATED,
                        maxAttempts)
                .stream()
                .map(messageMapper::map)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Message> findAllByRecipientAndStatusWithLock(String recipient, MessageStatus status) {
        return messageEntityRepository.findAllByRecipientAndStatus(recipient, messageMapper.mapStatus(status))
                .stream()
                .map(messageMapper::map)
                .collect(Collectors.toSet());
    }

    @Override
    public Message save(Message message) {
        var messageToSave = messageMapper.map(message);
        return messageMapper.map(messageEntityRepository.save(messageToSave));
    }

    @Override
    public Set<Message> saveAll(Set<Message> messages) {
        var messagesToSent = messages
                .stream()
                .map(messageMapper::map)
                .collect(Collectors.toSet());
        return StreamSupport.stream(messageEntityRepository.saveAll(messagesToSent).spliterator(), false)
                .map(messageMapper::map)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Message> findAll() {
        return StreamSupport.stream(messageEntityRepository.findAll().spliterator(), false)
                .map(messageMapper::map)
                .collect(Collectors.toSet());
    }

    @Override
    public long count() {
        return messageEntityRepository.count();
    }
}
