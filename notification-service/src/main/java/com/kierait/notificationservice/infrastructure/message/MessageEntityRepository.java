package com.kierait.notificationservice.infrastructure.message;

import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Set;

@Repository
interface MessageEntityRepository extends CrudRepository<MessageEntity, Long> {
    Set<MessageEntity> findAllByStatusAndAndAttemptsLessThanEqual(MessageStatus status, int attempts);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Set<MessageEntity> findAllByRecipientAndStatus(String recipient, MessageStatus status);
}
