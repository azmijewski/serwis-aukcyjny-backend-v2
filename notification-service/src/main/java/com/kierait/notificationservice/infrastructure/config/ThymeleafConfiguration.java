package com.kierait.notificationservice.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.StringTemplateResolver;

@Configuration
class ThymeleafConfiguration {
    @Bean
    @Primary
    ITemplateResolver templateResolver() {
        return new StringTemplateResolver();
    }
}
