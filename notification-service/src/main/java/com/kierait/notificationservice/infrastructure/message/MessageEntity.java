package com.kierait.notificationservice.infrastructure.message;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Entity
@Builder
@Table(name = "message")
class MessageEntity {
    private static final String SEQUENCE_NAME = "message_id_sequence";

    @Id
    @SequenceGenerator(name = SEQUENCE_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_NAME)
    private Long id;

    private String recipient;

    private LocalDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private MessageStatus status;

    private String payload;

    @Enumerated(EnumType.STRING)
    private MessageType type;

    private int attempts;

    private LocalDateTime completedAt;
}
