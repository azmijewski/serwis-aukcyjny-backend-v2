package com.kierait.notificationservice.infrastructure.messageconfig;

import com.kierait.notificationservice.domain.message.model.MessageType;
import com.kierait.notificationservice.domain.messageconfig.MessageConfigDao;
import com.kierait.notificationservice.domain.messageconfig.model.MessageConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Optional;

@Component
@Transactional(propagation = Propagation.MANDATORY)
@RequiredArgsConstructor
class MessageConfigDaoImpl implements MessageConfigDao {
    private final MessageConfigEntityRepository messageConfigEntityRepository;
    private final MessageConfigMapper messageConfigMapper;
    private final Clock clock;

    @Override
    public Optional<MessageConfig> findCurrentConfigForType(MessageType type) {
        var now = LocalDateTime.now(clock);
        return messageConfigEntityRepository.findByMessageTypeAndStartDateBetween(messageConfigMapper.mapType(type), now)
                .map(messageConfigMapper::map);
    }
}
