package com.kierait.notificationservice.infrastructure.message;

import com.kierait.notificationservice.domain.message.SendMessagesCommand;
import com.kierait.util.handler.CommandDispatcher;
import lombok.RequiredArgsConstructor;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class MessageSenderJob {
    private final CommandDispatcher commandDispatcher;

    @Scheduled(cron = "${sending-cron}")
    @SchedulerLock(name = "MessageSenderJob_sendMessages")
    @Transactional
    void sendMessages() {
        commandDispatcher.dispatch(new SendMessagesCommand());
    }
}
