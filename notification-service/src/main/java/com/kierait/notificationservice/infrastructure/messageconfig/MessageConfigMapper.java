package com.kierait.notificationservice.infrastructure.messageconfig;

import com.kierait.notificationservice.domain.messageconfig.model.MessageConfig;
import com.kierait.notificationservice.infrastructure.message.MessageType;
import com.kierait.util.mapper.MapperConfiguration;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfiguration.class)
interface MessageConfigMapper {
    MessageConfig map(MessageConfigEntity messageConfigEntity);
    MessageType mapType(com.kierait.notificationservice.domain.message.model.MessageType type);
}
