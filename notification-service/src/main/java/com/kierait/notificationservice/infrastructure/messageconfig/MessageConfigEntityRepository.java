package com.kierait.notificationservice.infrastructure.messageconfig;

import com.kierait.notificationservice.infrastructure.message.MessageType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
interface MessageConfigEntityRepository extends CrudRepository<MessageConfigEntity, Long> {
    @Query("select mc from MessageConfigEntity mc " +
            "where mc.type = :messageType " +
            "and mc.startDate <= :now " +
            "and mc.endDate >= : now")
    Optional<MessageConfigEntity> findByMessageTypeAndStartDateBetween(
            @Param("messageType") MessageType messageType,
            @Param("now")LocalDateTime now
    );
}
