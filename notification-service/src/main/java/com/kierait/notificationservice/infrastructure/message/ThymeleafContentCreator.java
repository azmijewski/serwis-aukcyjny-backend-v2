package com.kierait.notificationservice.infrastructure.message;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kierait.notificationservice.domain.exception.ContentPreparationException;
import com.kierait.notificationservice.domain.message.ContentCreator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
class ThymeleafContentCreator implements ContentCreator {
    private static final TypeReference<Map<String, Object>> TYPE_REFERENCE = new TypeReference<>() {};

    private final ITemplateEngine templateEngine;
    private final ObjectMapper objectMapper;

    @Override
    public String prepareContent(String content, String payload) throws ContentPreparationException {
        try {
            var context = new Context();
            var payloadAsMap = objectMapper.readValue(payload, TYPE_REFERENCE);
            context.setVariables(payloadAsMap);
            return templateEngine.process(content, context);
        } catch (JsonProcessingException e) {
            throw new ContentPreparationException();
        }
    }
}
