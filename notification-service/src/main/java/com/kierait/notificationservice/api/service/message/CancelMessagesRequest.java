package com.kierait.notificationservice.api.service.message;

import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
public class CancelMessagesRequest {
    @NotBlank
    String recipient;
}
