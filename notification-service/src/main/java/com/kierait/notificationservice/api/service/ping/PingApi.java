package com.kierait.notificationservice.api.service.ping;

import com.kierait.notificationservice.domain.ping.PingCommand;
import com.kierait.util.api.ApiCommandDispatcher;
import com.kierait.util.api.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/ping")
@Tag(name = "ping")
@RequiredArgsConstructor
public class PingApi {
    private final ApiCommandDispatcher apiCommandDispatcher;

    @PostMapping
    @Operation(description = "Ping test service",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Ping request successful", content = @Content(schema = @Schema(implementation = PingResponse.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class)))
            })
    ResponseEntity<?> ping(@RequestBody PingRequest pingRequest) {
        var command = new PingCommand(pingRequest.getMessage());
        return apiCommandDispatcher.dispatch(command, PingResponse::new);
    }
}
