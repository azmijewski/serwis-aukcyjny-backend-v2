package com.kierait.notificationservice.api.service.message;

import lombok.Value;

import java.time.LocalDateTime;

@Value
class ScheduleMessageResponse {
    Long id;
    LocalDateTime createdAt;
}
