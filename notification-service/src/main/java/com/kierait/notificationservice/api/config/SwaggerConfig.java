package com.kierait.notificationservice.api.config;

import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.OpenAPI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
class SwaggerConfig {
    @Bean
    OpenAPI api() {
        return new OpenAPI()
                .servers(List.of(new Server().url("/")))
                .info(
                        new Info()
                                .title("NOTIFICATION-SERVICE API")
                                .version("0.1.0")
                );
    }
}
