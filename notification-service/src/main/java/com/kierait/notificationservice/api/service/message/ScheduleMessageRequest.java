package com.kierait.notificationservice.api.service.message;

import com.kierait.notificationservice.domain.message.model.MessageType;
import com.kierait.util.validations.ValidEnumValue;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
class ScheduleMessageRequest {
    @NotBlank
    String recipient;

    @NotBlank
    String payload;

    @NotBlank
    @ValidEnumValue(enumClass = MessageType.class, message = "INVALID_MESSAGE_TYPE")
    String type;
}

