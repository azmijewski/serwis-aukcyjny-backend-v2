package com.kierait.notificationservice.api.service.message;

import com.kierait.notificationservice.domain.message.CancelMessagesForRecipientCommand;
import com.kierait.notificationservice.domain.message.ScheduleMessageCommand;
import com.kierait.util.mapper.MapperConfiguration;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfiguration.class)
interface MessageApiMapper {
    ScheduleMessageCommand map(ScheduleMessageRequest request);
    CancelMessagesForRecipientCommand map(CancelMessagesRequest request);
}
