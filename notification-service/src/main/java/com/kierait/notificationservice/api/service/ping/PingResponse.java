package com.kierait.notificationservice.api.service.ping;

import lombok.Value;

@Value
public class PingResponse {
    String message;
}
