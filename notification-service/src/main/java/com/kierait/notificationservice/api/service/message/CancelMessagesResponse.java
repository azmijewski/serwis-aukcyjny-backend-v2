package com.kierait.notificationservice.api.service.message;

import lombok.Value;

@Value
public class CancelMessagesResponse {
    Integer cancelled;
}
