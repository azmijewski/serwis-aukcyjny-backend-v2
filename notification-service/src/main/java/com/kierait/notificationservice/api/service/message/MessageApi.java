package com.kierait.notificationservice.api.service.message;

import com.kierait.util.api.ApiCommandDispatcher;
import com.kierait.util.api.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/messages")
@Tag(name = "message")
@RequiredArgsConstructor
class MessageApi {
    private final ApiCommandDispatcher apiCommandDispatcher;
    private final MessageApiMapper messageApiMapper;

    @PostMapping
    @Operation(summary = "Schedule message to send",
    responses = {
            @ApiResponse(responseCode = "200", description = "Message scheduled successfully", content = @Content(schema = @Schema(implementation = ScheduleMessageResponse.class))),
            @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class)))
    })
    ResponseEntity<?> scheduleMessage(@RequestBody @Valid ScheduleMessageRequest scheduleMessageRequest) {
        var command = messageApiMapper.map(scheduleMessageRequest);
        return apiCommandDispatcher.dispatch(command, message -> new ScheduleMessageResponse(message.getId(), message.getCreatedAt()));
    }

    @PutMapping
    @Operation(summary = "Cancel messages for recipient",
    responses = {
            @ApiResponse(responseCode = "200", description = "Message cancelled successfully", content = @Content(schema = @Schema(implementation = CancelMessagesResponse.class))),
            @ApiResponse(responseCode = "400", description = "Invalid request data", content = @Content(schema = @Schema(implementation = ApiErrorResponse.class)))
    })
    ResponseEntity<?> cancelMessagesForRecipient(@RequestBody @Valid CancelMessagesRequest cancelMessagesRequest) {
        var command = messageApiMapper.map(cancelMessagesRequest);
        return apiCommandDispatcher.dispatch(command, CancelMessagesResponse::new);
    }
}
