package com.kierait.auctionservice.api.config;

import com.kierait.util.annotations.EnableSecurityContextProvider;
import com.kierait.util.security.KeycloakGrantedAuthoritiesConverter;
import com.kierait.util.security.SecurityConstants;
import com.kierait.util.security.SecurityRequestMatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.List;

@EnableWebSecurity
@EnableSecurityContextProvider
public class WebSecurityConfig {
    private static final List<SecurityRequestMatcher> USER_PATHS = List.of(
    );


    private static final List<SecurityRequestMatcher> PERMIT_ALL_PATHS = List.of(
    );

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .requestMatchers(extract(USER_PATHS)).hasRole(SecurityConstants.USER_ROLE)
                .requestMatchers(extract(PERMIT_ALL_PATHS)).permitAll()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .oauth2ResourceServer()
                .jwt(jwtConfigurer -> jwtConfigurer.jwtAuthenticationConverter(jwtAuthenticationConverter()));

        return http.build();
    }

    private RequestMatcher[] extract(List<SecurityRequestMatcher> securityRequestMatchers) {
        return securityRequestMatchers.stream()
                .flatMap(securityRequestMatcher -> securityRequestMatcher.toAntPathMatcher().stream())
                .toArray(RequestMatcher[]::new);
    }

    private JwtAuthenticationConverter jwtAuthenticationConverter() {
        var converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(new KeycloakGrantedAuthoritiesConverter());
        return converter;
    }
}
