package com.kierait.auctionservice;

import com.kierait.util.annotations.EnableDispatchers;
import com.kierait.util.annotations.EnableFeignOAuth2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@ConfigurationPropertiesScan(basePackageClasses = AuctionServiceApplication.class)
@EnableFeignClients
@EnableFeignOAuth2
@EnableDispatchers
@EnableAsync
public class AuctionServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuctionServiceApplication.class, args);
    }

}
